<?php

/**
 * Plugin Name: My Related Posts
 * Plugin URI: https://www.nigelstewart.tech/plugins/relatedposts
 * Author Name: Nigel Stewart
 * Author URI: https://www.nigelstewart.tech
 * Description: Add related posts to single post
 * Version: 0.1-beta
 */

if(!function_exists('cs_related_posts')){

    function cs_related_posts($atts, $content, $slug)
    {

        if($atts['title']) {
            $title = $atts['title'];
        }

        $similar = $atts['similar'];
        $posts = get_posts(array('include' => $similar));

        $html = '<ul class="rel-nav"><h4 style="margin-bottom: 5px; padding-bottom: 0;">Related Posts:</h4>';
        foreach($posts as $post){
            $permalink = get_the_permalink($post->ID);
            $html .= "<li><a href=\"$permalink\">$post->post_title</a></li>";
        }
        $html .= "</ul>";

        return $html;
    }

} add_shortcode('related', 'cs_related_posts');
